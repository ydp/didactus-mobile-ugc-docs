# Didacuts UGC offline persistence

Endpoints provided by a native Didactus application on mobile devices in order to allow players saving and obtaining User-Generated Content data in the offline mode.

See [generated Swagger documentation](http://editor.swagger.io/#/?import=https:%2F%2Fbitbucket.org%2Fydp%2Fdidactus-mobile-ugc-docs%2Fraw%2Fmaster%2Fswagger.yaml) from the `swagger.yml` from this repository.